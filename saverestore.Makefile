## The following lines are mandatory, please don't change them.
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile
include $(E3_REQUIRE_CONFIG)/DECOUPLE_FLAGS


ifneq ($(strip $(AUTOSAVE_DEP_VERSION)),)
asyn_VERSION=$(AUTOSAVE_DEP_VERSION)
endif


APP:=src
APPSRC:=$(APP)
APPDB:=db


USR_INCLUDES += -I$(where_am_I)$(APPSRC)

TEMPLATES += $(wildcard $(APPDB)/*.db)
TEMPLATES += $(wildcard $(APPDB)/*.template)

SOURCES   += $(wildcard $(APPSRC)/*.c)

DBDS += dbd/SaveRestore.dbd


SCRIPTS += $(wildcard ../iocsh/*.iocsh)


.PHONY: db 
db: 

.PHONY: vlibs
vlibs:
